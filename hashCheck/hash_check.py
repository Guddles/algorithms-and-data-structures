with open ("out.txt", 'r') as f:
    lines = f.readlines()
    size = len(lines)
def conv(a):
    a = a.split()
    last_bit = int(a[-1])
    k = 0
    for i in range(len(a)-1):
        num = bin(int(a[i])).count('1')
        # print(num)
        k += num
    if k % 2 == last_bit:
        return False
    else:
        return True
count = 0
for i in lines:
    if conv(i):
        count += 1
print(count / size * 100, "%")