#include<iostream>
#define _USE_MATH_DEFINES
#include<cmath>
#include<ctime>
#include<fstream>

using namespace std;

int main(int argc, char* argv[]){

    ofstream file;
    file.open("out.txt");
    srand(time(NULL));
    int n = atoi(argv[1]);
    file<<n<<endl;
    for (int i =0 ; i<n; i++)
    {
        float x = -M_PI + (float)rand() / (float)(RAND_MAX / (M_PI - (-M_PI)));
        float y = -M_PI + (float)rand() / (float)(RAND_MAX / (M_PI - (-M_PI)));
        float z = -M_PI + (float)rand() / (float)(RAND_MAX / (M_PI - (-M_PI)));
        file<< x<< " "<<y<<" "<<z<<" "<< endl;
    }
    file.close();
    return 0;    
}