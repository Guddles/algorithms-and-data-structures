#include <iostream>
#include <fstream>
#include <cmath>

using namespace std;

int main(){
    int n;
    string line;
    ifstream file("out.txt");
    file >> n ;
    for (int i = 0; i < n; i++){
        float x, y, z;
        file >>x >>y >>z;
        float angle = 2 * sin(x) * sin(y) + cos(z);
        cout<<"angel = "<<angle<<endl;
    }
    file.close();
}
