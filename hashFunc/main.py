import os
import time
import random


def crc_hash(string):
    hash = 0
    for i in string:
        highorder = hash & 0xf8000000
        hash = hash << 5
        hash = hash ^ (highorder >> 27)
        hash = hash ^ ord(i)
    return hash
def pjw_hash(string):
    hash = 0
    for i in string:
        hash = (hash << 4) + ord(i)
        g = hash & 0xf0000000
        if (g != 0):
            hash = hash ^ (g >> 24)
            hash = hash ^ g
    return hash
def buz_hash(string):
    hash = 0
    for i in string:
        highorder = hash & 0x80000000
        hash = hash << 1
        hash = hash ^ (highorder >> 31)
        random.seed(ord(i))
        hash = hash ^ random.randint(0,1)
    return hash
def read_file(path):
    file_list = []
    files = os.listdir(path)
    for file_name in files:
        with open(path + '/' + file_name, "r") as f:
            file_ = f.read()
            file_list.append(file_)
    return file_list

def find_dups(hash_func, file_list):
    start = time.time()
    dups = set(map(hash_func, file_list))
    execution_time = time.time() - start
    print("Время выполнения: ", execution_time)
    print("Дубликатов: ", len(file_list) - len(dups))



print("crc_hash:")
texts = read_file("./out")
find_dups(crc_hash, texts)
print("pjw_hash:")
find_dups(pjw_hash, texts)
print("buz_hash:")
find_dups(buz_hash, texts)
print("python_hash: ")
find_dups(hash, texts)