#include<iostream>
#include<string>


class NDArray{
    private: 
        int* arr;
        int shape[2];
        int size;

    public:
        NDArray(int y, int x) {
            shape[0] = y;
            shape[1] = x;
            size = x * y;
            arr = new int[size];
        };
        NDArray(int x, int y, int fill){
            shape[0] = x;
            shape[1] = y;
            size = _prod();
            arr = new int[size];
            to_fill(fill);
        };

        int _prod();
        void to_fill(int fill);
        void _null();
        void ones();
        void zeroes();
        void rand_gen(int min, int max);
        int sum();
        NDArray transpos();
        NDArray op(char operator_, const NDArray other){
            if((shape[0]) == other.shape[0] && (shape[1]) == other.shape[1]){
                NDArray n_arr(shape[0], shape[1]);
                for (int i =0; i <size; i++){
                    switch (operator_)
                    {
                    case '+':
                        n_arr.arr[i] = arr[i] + other.arr[i];
                        break;
                    case '*':
                        n_arr.arr[i] = arr[i] * other.arr[i];
                        break;
                    case '-':
                        n_arr.arr[i] = arr[i] - other.arr[i];
                        break;
                    case '/':
                        n_arr.arr[i] = arr[i] / other.arr[i];
                        break;
                    default:
                        std::cout<<"Unknow operator";
                        break;
                    }
                }
                return n_arr;
            }
            else{
                std::cout<<"Wrogn shape"<<std::endl;
            }
        };
        void matmul(NDArray &other);
        std::string display();
        int get_index(int x, int y);

        // перегрузка операторов
        const NDArray operator+(const NDArray& other_array){
            return op('+', other_array);
        };
        const NDArray operator-(const NDArray& other_array){
            return op('-', other_array);
        };
        const NDArray operator/(const NDArray& other_array){
            return op('/', other_array);
        };
        const NDArray operator*(const NDArray& other_array){
            return op('*', other_array);
        };


};
// перегрузка операторов
std::ostream& operator<<(std::ostream &os, NDArray arr) {
    os << arr.display();
    return os;
}
// получение размера
int NDArray::_prod(){
    size = shape[0] * shape[1];
    return size;
}
// заполенение
void NDArray::to_fill(int fill){
    for (int i =0; i< size; i++){
        arr[i] = fill;
    }
}
// создание пустого
void NDArray::_null(){
    arr = new int[size];
}
// создание из единиц
void NDArray::zeroes(){
    to_fill(1);
}
// создание из нулей
void NDArray::ones(){
    to_fill(0);

}
// генерация со случайными значениями
void NDArray::rand_gen(int r_min, int r_max){
    for (int i = 0; i < size; i++){
        arr[i] = rand() % r_max + r_min;
    }
}
//функция вывода
std::string NDArray::display(){
    std::string res;
    for (int i =0 ; i <shape[0]; i++){
        for (int j = 0; j<shape[1]; j++){
            res += std::to_string(arr[get_index(i,j)]) + " ";
        }
        res += '\n';
    }
    return res;
}
int NDArray::sum(){
    int sum = 0;
    for (int i =0;  i < size; i++){
        sum += arr[i];
    }
    return sum;
}
 NDArray NDArray::transpos(){
    NDArray tranM(shape[1], shape[0]);
    for (int i = 0; i <shape[1]; i++){
        for (int j =0; j<shape[0]; i++){
            tranM.arr[tranM.get_index(i,j)] = arr[get_index(j,i)];
        }
    }
    return tranM;
}
// void NDArray::matmul(NDArray &other){
//     NDArray multyM(shape[0], other.shape[1]);
//     for (int i =0; i < shape[0]; i++){
//         for (int j = 0; j< shape[1]; j++){
//             multyM.arr[get_index(i,j)] = 

//         }
//     }
// }


int NDArray::get_index(int x, int y){
    return(x*shape[0] + y);
}

int main(){

    std::cout<<"Make empty array 3x3: "<<std::endl;
    NDArray arr(3,3);
    std::cout<<arr<<std::endl;
    std::cout<<"Make random matrix: "<<std::endl;
    arr.rand_gen(0,10);
    std::cout<<arr<<std::endl;
    std::cout<<"Make zeroes array: "<<std::endl;
    NDArray brr (3,3,0);
    std::cout<<brr<<std::endl;
    std::cout<<"Make ones array: "<<std::endl;
    NDArray crr (3,3,1);
    std::cout<<crr<<std::endl;
    
    // сложение/деление/умножение/вычитание

    NDArray aarr(3,3), bbarr(3,3);
    aarr.rand_gen(0,10);
    bbarr.rand_gen(0,10);
    std::cout<<"Sum: "<<std::endl<<aarr + bbarr<<std::endl;
    std::cout<<"Sub: "<<std::endl<<aarr - bbarr<<std::endl;
    std::cout<<"Mul: "<<std::endl<<aarr * bbarr<<std::endl;
    std::cout<<"Div: "<<std::endl<<aarr + bbarr<<std::endl;



    return 0;
}